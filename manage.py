#!/usr/bin/env python
# -*- coding: utf-8 -*-

import getpass
from flask import Flask, current_app
from flask.ext.script import Server, Shell, Manager, Command, prompt_bool
from werkzeug.contrib.fixers import ProxyFix

from skeleton.extensions import db, cache
from skeleton import create_app

app = create_app('config/development.conf')
app.wsgi_app = ProxyFix(app.wsgi_app)

manager = Manager(app)
manager.add_command("runserver", Server('127.0.0.1', port=5000))


def _make_context():
    return dict(db=db)

manager.add_command("shell", Shell(make_context=_make_context))


@manager.command
def createall():
    db.create_all()


@manager.command
def dropall():
    if prompt_bool:
        db.drop_all()

if __name__ == "__main__":
    manager.run()
