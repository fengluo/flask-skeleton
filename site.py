#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""docstring
"""

from werkzeug.contrib.fixers import ProxyFix

from winterfell import create_app

app = create_app('config.conf')
app.wsgi_app = ProxyFix(app.wsgi_app)
